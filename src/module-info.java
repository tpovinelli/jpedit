module jpedit {
  requires javafx.base;
  requires javafx.controls;
  requires TomUtils;
  requires java.logging;
  requires org.jetbrains.annotations;
  requires kotlin.stdlib;

  exports com.tom.jpedit;
  exports com.tom.jpedit.gui;
  exports com.tom.jpedit.gui.confirmation;
  exports com.tom.jpedit.gui.dialog;
  exports com.tom.jpedit.gui.find;
  exports com.tom.jpedit.logging;
  exports com.tom.jpedit.handlers;
  exports com.tom.jpedit.handlers.edit;
  exports com.tom.jpedit.handlers.file;
  exports com.tom.jpedit.handlers.misc;
  exports com.tom.jpedit.listeners;
  exports com.tom.jpedit.util;
  exports com.tom.jpedit.plugins;
  exports com.tom.jpedit.plugins.components;
  exports com.tom.jpedit.workers;
}