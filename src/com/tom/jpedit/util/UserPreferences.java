package com.tom.jpedit.util;

import com.tom.jpedit.logging.JPLogger;
import javafx.scene.text.Font;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.util.Properties;

public class UserPreferences extends Properties {

  public static final String FONT_FAMILY = "font-family";
  public static final String FONT_SIZE = "font-size";
  public static final String FULL_PATH_SHOWING = "is-full-path-showing";
  public static final String FONT_STYLE = "font-style";
  public static final String SYSTEM_FAMILY_NAME = "System";
  public static final String TRUE = "true";
  public static final String FALSE = "false";
  public static final String DEFAULT_SIZE = "17.0";
  public static final String AUTOSAVE_PERIOD_MILLIS = "autosave-period-millis";
  public static final long DEFAULT_AUTOSAVE_PERIOD_MILLIS = Duration.ofMinutes(5).toMillis();

  public void setAutosavePeriodMillis(long millis) {
    setProperty(AUTOSAVE_PERIOD_MILLIS, String.valueOf(millis));
  }

  public long getAutosavePeriodMillis() {
    return JPUtil.parseLongOr(getProperty(AUTOSAVE_PERIOD_MILLIS), DEFAULT_AUTOSAVE_PERIOD_MILLIS);
  }

  public Font getPreferredFont() {
    JPLogger.getAppLog().info("Font family " + getProperty(FONT_FAMILY, SYSTEM_FAMILY_NAME));
    return Font.font(
        getProperty(FONT_FAMILY, SYSTEM_FAMILY_NAME),
        JPUtil.parseDoubleOr(getProperty(FONT_SIZE, DEFAULT_SIZE), 17.0)
    );
  }

  public void setPreferredFont(@NotNull Font font) {
    String family = font.getName();
    String size = Double.toString(font.getSize());
    String style = font.getStyle();
    JPLogger.getAppLog().info("Setting user preferred font to " + family + " at " + size + " with style " + style);
    setProperty(FONT_STYLE, style);
    setProperty(FONT_FAMILY, family);
    setProperty(FONT_SIZE, size);
  }

  public boolean isFullPathShowing() {
    return getProperty(FULL_PATH_SHOWING, FALSE).equalsIgnoreCase(TRUE);
  }

  public void setFullPathShowing(boolean showing) {
    JPLogger.getAppLog().info("Setting full path showing to " + showing);
    setProperty(FULL_PATH_SHOWING, Boolean.toString(showing));
  }
}
