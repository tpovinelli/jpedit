package com.tom.jpedit.gui.menu;

import com.tom.jpedit.gui.JPEditWindow;
import com.tom.jpedit.handlers.file.*;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

public class JPEditFileMenu extends JPEditMenu {
  public JPEditFileMenu(JPEditWindow owner, String s) {
    super(owner, s);
    final MenuItem newItem = new MenuItem("New");
    final MenuItem newWindowItem = new MenuItem("New Window...");
    final MenuItem openItem = new MenuItem("Open...");
    final Menu openRecentMenu = new JPEditFileRecentItemsMenu(owner, "Open recent");
    final MenuItem saveItem = new MenuItem("Save");
    final MenuItem saveAsItem = new MenuItem("Save as...");
    final MenuItem duplicateItem = new MenuItem("Duplicate Window");
    final MenuItem printItem = new MenuItem("Print...");
    final MenuItem clearRecentMenuItem = new MenuItem("Clear Recent Files");
    final MenuItem toggleFullPathItem = new MenuItem("Toggle Full Path in Title");
    final MenuItem quitItem = new MenuItem("Close");

    newItem.setOnAction(new NewActionHandler(owner));
    newWindowItem.setOnAction(new NewWindowActionHandler(owner));
    openItem.setOnAction(new OpenActionHandler(owner));
    saveItem.setOnAction(new SaveActionHandler(owner));
    saveAsItem.setOnAction(new SaveAsActionHandler(owner));
    duplicateItem.setOnAction(new DuplicateWindowActionHandler(owner));
    printItem.setOnAction(new PrintActionHandler(owner));
    clearRecentMenuItem.setOnAction(new ClearRecentFilesHandler(owner));
    toggleFullPathItem.setOnAction(new ToggleFullPathHandler(owner));
    quitItem.setOnAction(new CloseWindowActionHandler(owner));

    getItems().addAll(
        newItem,
        newWindowItem,
        openItem,
        openRecentMenu,
        saveItem,
        saveAsItem,
        duplicateItem,
        new SeparatorMenuItem(),
        printItem,
        new SeparatorMenuItem(),
        clearRecentMenuItem,
        new SeparatorMenuItem(),
        toggleFullPathItem,
        new SeparatorMenuItem(),
        quitItem
    );
  }
}
