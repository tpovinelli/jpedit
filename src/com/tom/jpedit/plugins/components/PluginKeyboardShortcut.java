package com.tom.jpedit.plugins.components;

import com.tom.jpedit.gui.JPEditWindow;
import com.tom.jpedit.logging.JPLogger;
import com.tom.jpedit.plugins.JPEditPlugin;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public abstract class PluginKeyboardShortcut implements PluginOwnedComponent, Cloneable {
  private final boolean shiftDown;
  private final boolean controlDown;
  private final boolean shortcutDown;
  private final boolean altDown;
  private final boolean metaDown;
  private final KeyCode keyCode;
  private JPEditWindow owner;
  public PluginKeyboardShortcut(
      boolean shiftDown,
      boolean controlDown,
      boolean shortcutDown,
      boolean altDown,
      boolean metaDown,
      KeyCode keyCode
  ) {
    this.shiftDown = shiftDown;
    this.controlDown = controlDown;
    this.shortcutDown = shortcutDown;
    this.altDown = altDown;
    this.metaDown = metaDown;
    this.keyCode = keyCode;
  }

  @Override
  public PluginKeyboardShortcut clone() {
    try {
      return (PluginKeyboardShortcut) super.clone();
    } catch (CloneNotSupportedException e) {
      throw new RuntimeException(e);
    }
  }

  public EventHandler<KeyEvent> asHandler() {
    return event -> {
      JPLogger.getAppLog().info("Code is " + getKeyCode() + " and event is " + event.getCode());
      if (event.isControlDown() == isControlDown() && event.isShiftDown() == isShiftDown() && event.isControlDown() == isControlDown() &&
          event.isShortcutDown() == isShortcutDown() && event.isMetaDown() == isMetaDown() && event.getCode() == getKeyCode()) {
        JPLogger.getAppLog().info("Into the handler");
        PluginKeyboardShortcut.this.handle(event);
      }
    };
  }

  public abstract void handle(KeyEvent event);

  public boolean isShiftDown() {
    return shiftDown;
  }

  public boolean isControlDown() {
    return controlDown;
  }

  public boolean isShortcutDown() {
    return shortcutDown;
  }

  public boolean isMetaDown() {
    return metaDown;
  }

  public KeyCode getKeyCode() {
    return keyCode;
  }

  public JPEditWindow getOwner() {
    return owner;
  }

  public void setOwner(JPEditWindow owner) {
    this.owner = owner;
  }

  public boolean isAltDown() {
    return altDown;
  }
}
