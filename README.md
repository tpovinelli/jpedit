# JPEdit

JPEdit is a simple Notepad.exe inspired text editor written in Java using JavaFX. It was done as a sort of just-for-fun project by Thomas Povinelli.

I was writing a lot of Kotlin for a while, and I really missed doing plain Java, so I decided to put together a complete application in only Java 
(thought I did cheat a little by using my TomUtils library which is a Kotlin library). 

Most of the functionality here is taken from Notepad on Microsoft Windows&trade;, but JPEdit has a plugin system that is notably lacking 
from Notepad. 

You can find out more about how to make plugins in the Plugins.md file in this repo. As a warning, the plugin process is experimental and somewhat unpolished. You will have to set up and build multiple Java projects with dependencies and create JARs in order to use it. 

I don't have many plans to develop for this pet project, but I did recently update it to using the Java Module System and compatibility with 
Java 18. 
